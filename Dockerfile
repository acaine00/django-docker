FROM ubuntu:22.04@sha256:35fb073f9e56eb84041b0745cb714eff0f7b225ea9e024f703cab56aaa5c7720

RUN apt-get update && apt-get upgrade -y && apt-get install -y \
    pkg-config \
    python3 \
    python3-dev \
    python3-venv \
    python3-pip \
    python-is-python3 \
    libmysqlclient-dev \
    mysql-client \
  && rm -rf /var/lib/apt/lists/*


WORKDIR /home/django
RUN python -mvenv .venv
ENV PATH="/home/django/.venv/bin:$PATH"

COPY requirements.txt /tmp/
RUN pip install --upgrade pip
RUN pip install -r /tmp/requirements.txt

ENV DJANGO_SECRET_KEY="thisisasecret"

RUN groupadd -r django && useradd --no-log-init -r -g django django

RUN mkdir /usr/local/django-media
RUN chown -R django: /home/django /usr/local/django-media

USER django

CMD ["/bin/bash"]
